# README #

Run Glassfish. Then Deploy the project using Add/Remove of Server panel of Glassfish. Read the console values.

**Note:** Please monitor the console, the Async task is writing to console after main method writes :) Also as you see, you can pass a variable to async task to maybe process it.

### Used Technologies for all projects ###

* Java SE,
* Java EE,
* 3rd party Java frameworks / libraries.

### Requirements ###

* Eclipse IDE
* JDK 1.8
* Glassfish 4

I hope you enjoy :)