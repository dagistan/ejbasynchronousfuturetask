package dagistan.sample.futuretask;

import java.util.concurrent.Future;

import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.Stateless;

@Stateless
public class AsyncImpl implements Async{
	
	@Asynchronous
	public Future<String> asyncTask(String value) throws Exception {
		
		System.out.println("*** This is Asynchronous Method... :)");
		System.out.println("--");
		System.out.println(value);
		System.out.println("--");
		
		return new AsyncResult<String>("Async Task Succeeded.");
	}

}
