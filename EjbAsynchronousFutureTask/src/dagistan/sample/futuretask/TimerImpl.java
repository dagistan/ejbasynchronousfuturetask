package dagistan.sample.futuretask;

import java.util.concurrent.Future;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

@Stateless
public class TimerImpl {

	@EJB
	private Async async;

	@Schedule(second = "*/5", minute = "*", hour = "*", persistent = false)
	public void makeAsyncCall() {
		try {
			System.out.println("Timer started");
			System.out.println("--");

			Future<String> asyncTask = async.asyncTask("Async method can take an input variable like that");

			System.out.println("Timer finished.");
			System.out.println("--");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
