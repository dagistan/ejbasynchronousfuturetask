package dagistan.sample.futuretask;

import java.util.concurrent.Future;

public interface Async {

	public Future<String> asyncTask(String value) throws Exception;

}
